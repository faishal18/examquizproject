import jwt from "jsonwebtoken";
import messages from "../../../localization/en";
import logger from "../../../utils/logger";
import util from "../../../utils/util";
import conf from "../../../config/development";

// import models from "../../database";

/**
 * Checking if document exist with reference
 * @property {object} Model - seuelize model object.
 * @property {object} data - Object to check duplicate for .
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {boolean/document}
 */
async function checkDuplicate({ Model, data, autoFormat = false }) {
  // Checking if doc exist
  const existingRefDoc = await findByUserName({
    Model,
    data,
    autoFormat: false,
  });
  // Returning true if exist
  if (existingRefDoc.error) {
    return true;
  }
  if (existingRefDoc !== null) {
    // Returning formatted response if autoFormat true
    if (autoFormat) {
      const errObj = {
        error: {
          status: 409,
          message: messages.ALREADY_EXIST,
          conflictObj: existingRefDoc,
        },
      };
      return errObj;
    }

    // Otherwise returned db object
    return existingRefDoc;
  }
  return true;
}
/**
 * Finding document with user Name
 * @property {object} Model - sequelize model object.
 * @property {object} user - The userName object.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function findByUserName({ Model, data, autoFormat = true }) {
  // Getting document with reference
  const existingRefDoc = await Model.findOne({
    where: data,
  });
  // Returning doc if exist
  if (existingRefDoc !== null) {
    // Returning formatted response if autoFormat true
    if (autoFormat) {
      return {
        status: 200,
        data: existingRefDoc,
        message: messages.SUCCESSFULL,
      };
    }
    // Otherwise returned db object
    return existingRefDoc;
  }

  // Returning error obj if does not exist
  const errObj = {
    error: {
      status: 404,
      message: messages.NOT_FOUND,
    },
  };
  return errObj;
}

/**
 * Creating document
 * @property {object} Model - sequelize model object.
 * @property {object} data - document properties.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function create({ Model, data, autoFormat = true }) {
  const savedDoc = await Model.create(data);

  // Returning formatted response if autoFormat true
  if (autoFormat) {
    return {
      status: 201,
      data: savedDoc,
      message: messages.CREATED,
    };
  }

  // Otherwise returned db object
  return savedDoc;
}

/**
 * delete document
 * @property {object} Model - Mongoose model object.
 * @property {string} id - document id to be removed.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function removeById({ Model, id, autoFormat = true }) {
  // Getting and deleting document.
  const filterCriteria = { id };
  const deletedDoc = await Model.destroy({
    where: filterCriteria,
  });

  // Returning error returned from destroy method
  if (deletedDoc.error) {
    return deletedDoc;
  }

  // Returning formatted response if autoFormat true
  if (autoFormat) {
    return {
      status: 200,
      message: messages.DELETED,
    };
  }

  // Otherwise returned db object
  return deletedDoc;
}

/**
 * Finding document with id
 * @property {object} Model - Sequelize model object.
 * @property {string} id - document id.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function findById({ Model, id, autoFormat = true }) {
  // Getting document with id
  const existingDoc = await Model.findOne({
    where: {
      id,
    },
    attributes: { exclude: ["createdAt", "updatedAt"] },
  });
  // Returning doc if exist
  if (existingDoc !== null) {
    // Returning formatted response if autoFormat true
    if (autoFormat) {
      return {
        status: 200,
        data: existingDoc,
        // message: messages.SUCCESSFULL,
      };
    }

    // Otherwise returned db object
    return existingDoc;
  }

  // Returning error obj if does not exist
  const errObj = {
    error: {
      status: 404,
      message: messages.NOT_FOUND,
    },
  };
  // if (errKey) {
  //   errObj.error.data = [
  //     {
  //       [errKey]: messages.NOT_FOUND,
  //     },
  //   ];
  // }
  return errObj;
}

/**
 * Updating document
 * @property {Object} Model - Sequelize model object.
 * @property {object} id - id which needs to be updated.
 * @property {object} data - document properties.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function updateExisting({ Model, id, data, autoFormat = true }) {
  // Updating existing document with new data
  const savedDoc = await Model.update(data, {
    where: {
      id,
    },
  });

  // Returning error obj if does not exist
  if (savedDoc === null) {
    return {
      error: {
        status: 404,
        message: messages.NOT_FOUND,
      },
    };
  }

  // Returning formatted response if autoFormat true
  if (autoFormat) {
    return {
      status: 200,
      data: savedDoc,
      message: messages.UPDATED,
    };
  }

  // Otherwise returned db object
  return savedDoc;
}

/**
 * Finding documents with provided query params
 * @property {object} Model - Sequelize model object.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {documents[]}
 */
async function find({ Model, autoFormat = true }) {
  // preparing query filters
  const filterCriteria = {
    status: "ACTIVE",
  };

  // Getting documents with available filters
  const docs = await Model.all(filterCriteria);

  // Returning formatted response if autoFormat true
  if (autoFormat) {
    return {
      status: 200,
      data: docs,
      message: messages.SUCCESSFULL,
    };
  }

  // Otherwise returned db object
  return docs;
}

async function verifyToken(req, res, next) {
  const afterSplit = req.cookies.token;
  const decoded = await jwt.verify(afterSplit, conf.jwt.secret);
  if (decoded.isAdmin === true) {
    next();
    // return true;
  } else {
    return res.json({ message: "please login as admin" });
  }
}

export default {
  checkDuplicate,
  findByUserName,
  create,
  removeById,
  findById,
  updateExisting,
  find,
  verifyToken,
};
