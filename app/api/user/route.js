import express from "express";
import c from "../../../utils/controlHandler";
import controller from "./controller";
// import commonService from "../../helpers/services/commonService";

const router = express.Router();

// router.use(c(commonService.verifyToken, ({ headers }) => [headers]));

router
  .route("/login")
  // user login (accessed at POST /api/user/login)
  .post(c(controller.authenticate, ({ body }) => [body]));
router
  .route("/signUp")
  // user sign up (accessed at POST /api/user/signUp)
  .post(c(controller.create, ({ body }) => [body]));
router
  .route("/:id([0-9]+)")
  // update user (accessed at PUT /api/user/:id)
  .put(c(controller.update, ({ params, body }) => [params, body]))
  // remove user (accessed at DELETE /api/user/:id)
  .delete(c(controller.remove, ({ params }) => [params]))
  // get user (accessed at GET /api/user/:id)
  .get(c(controller.get, ({ params }) => [params]));

export default router;
