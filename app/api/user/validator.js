import Joi from "joi";
import { join } from "path";
import constants from "../../helpers/constants";
import commonValidator from "../../helpers/validators/commonValidator";

export default {
  // GET /api/user/:id
  get: Joi.object({
    params: Joi.object({
      id: commonValidator.validMongoId,
    })
      .xor("id", "feedSource")
      .and("feedSource", "key"),
  }),

  // GET /api/user
  list: Joi.object({
    query: Joi.object({
      sortBy: Joi.array().items(Joi.any().valid(constants.sortByKeys)),
      limit: Joi.number().integer(),
      skip: Joi.number().integer(),
      tags: Joi.array().items(
        Joi.any()
          .valid(constants.teamTags)
          .required(),
      ),
      approvalStatus: Joi.array().items(
        Joi.any()
          .valid(constants.approvalStatusTypes)
          .required(),
      ),
    }),
  }),

  // POST /api/user
  create: Joi.object({
    body: Joi.object({
      userName: commonValidator.normalStr.required(),
      emailId: Joi.string().email(),
      phoneNo: Joi.string().regex(/[0-9]{10}/gm),
      password: Joi.string().regex(/[a-zA-Z0-9]{3,30}/),
      isAdmin: Joi.boolean(),
      isStudent: Joi.boolean(),
    }),
  }),

  // PUT /api/user/:id
  update: Joi.object({
    params: Joi.object({
      id: commonValidator.validMysqlId.required(),
    }),
    body: Joi.object({
      userName: commonValidator.normalStr.required(),
      emailId: Joi.string().email(),
      phoneNo: Joi.string().regex(/[0-9]{10}/gm),
      password: Joi.string().regex(/[a-zA-Z0-9]{3,30}/),
      isAdmin: Joi.boolean(),
      isStudent: Joi.boolean(),
    }),
  }),

  // DELETE /api/user/:id
  remove: Joi.object({
    params: Joi.object({
      id: commonValidator.validMysqlId.required(),
    }),
  }),
};
