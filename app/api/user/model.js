export default (sequelize, type) =>
  sequelize.define(
    "user",
    {
      id: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      userName: type.STRING,
      emailId: type.STRING,
      phoneNo: type.INTEGER,
      password: type.INTEGER,
      isAdmin: type.BOOLEAN,
      isStudent: type.BOOLEAN,
    },
    { freezeTableName: true },
  );
