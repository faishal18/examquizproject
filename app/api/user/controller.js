import util from "../../../utils/util";
import validator from "./validator";
import service from "./service";

/**
 * login User
 * @property {string} params.userName - user name for login.
 * @property {string} params.password - user's password for login.
 * @returns {Object}
 */
async function authenticate(params) {
  // Validating param
  // const validParam = await validator.get.validate({ params });
  const { userName } = params;

  // Getting team details
  let existingDoc;
  if (userName) {
    existingDoc = await service.login(params);
  }
  return existingDoc;
}

/**
 * Create new User
 * @property {string} body.userName - The name of User.
 * @property {string} body.emailId - User 's emailId.
 * @property {string} body.phoneNo - user's phone No.
 * @property {array} body.password - user's password.
 * @returns {Object}
 */

async function create(body) {
  // Validating body
  const validData = await validator.create.validate({ body });

  const validBody = validData.body;
  const promiseList = [];

  if (validBody.userName) {
    // Checking if document exist with same reference
    promiseList.push(service.checkDuplicate({ userName: validBody.userName }));
  }
  // Waiting for promises to finish
  const promiseListResp = await Promise.all(promiseList);

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(promiseListResp);

  const encryptedPass = await service.encryptPassword(validBody.password);
  validBody.password = encryptedPass;

  // Creating new team
  const newDoc = await service.create({ data: validBody });

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(newDoc);

  return newDoc;
}

/**
 * Delete User.
 * @property {string} params.id - User Id.
 * @returns {Object}
 */
async function remove(params) {
  // Validating param
  const validParam = await validator.remove.validate({ params });
  const { id } = validParam.params;

  // Deleting the user
  const deletedDoc = await service.removeById({ id });

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(deletedDoc);

  return deletedDoc;
}

/**
 * Get User
 * @property {string} params.id - user Id.
 * @returns {Object}
 */
async function get(params) {
  const validParam = await validator.remove.validate({ params });
  const { id } = validParam;

  // find document by Id
  const foundDoc = await service.findById({ id });

  // Throwing error if promise response has an error object.
  util.FilterErrorAndThrow(foundDoc);
  return foundDoc;
}

/**
 * Update existing user
 * @property {string} body.userName - The name of User.
 * @property {string} body.emailId - User 's emailId.
 * @property {string} body.phoneNo - user's phone No.
 * @property {array} body.password - user's password.
 * @returns {Object}
 */
async function update(params, body) {
  // Validating param
  const validParam = await validator.update.validate({ params });

  const { id } = validParam.params;

  // Getting user object to be updated
  const existingDoc = await service.findById({ id, autoFormat: false });

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(existingDoc);

  // Validating body
  const validData = await validator.update.validate({ body });

  const validBody = validData.body;
  const promiseList = [];

  // Waiting for promises to finish
  const promiseListResp = await Promise.all(promiseList);

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(promiseListResp);

  // Updating new data to document
  const savedDoc = await service.updateExisting({
    data: validBody,
    id,
  });

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(savedDoc);

  return savedDoc;
}

export default { authenticate, create, remove, get, update };
