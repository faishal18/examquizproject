import jwt from "jsonwebtoken";
import crypto from "crypto";
import models from "../../database";
// import util from "../../../utils/util";
import commonService from "../../helpers/services/commonService";
import messages from "../../../localization/en";
import conf from "../../../config/development";

/**
 * User Login
 * @property {object} data - login User Name
 * @returns {object}
 */
async function login(data) {
  let res = "";
  const userName = {
    userName: data.userName,
  };
  // Finds user by user name
  const result = await models.user.findOne({ where: userName });
  if (!result) {
    res = {
      success: false,
      message: messages.USER_NOT_FOUND,
    };
  } else if (result) {
    const password = await decryptPassword(result.password);
    if (password !== data.password) {
      res = {
        success: false,
        message: messages.WRONG_PASSWORD,
      };
    } else {
      const payload = {
        userName: result.userName,
        isAdmin: result.isAdmin,
        isStudent: result.isStudent,
      };
      const token = jwt.sign(payload, conf.jwt.secret, {
        expiresIn: 43200, // expires in 12 hours
      });
      // return the information including token as JSON
      res = {
        success: true,
        message: messages.LOGIN_SUCCESS,
        token,
      };
    }
  }
  return res;
}

/**
 * Creating document
 * @property {object} data - document properties.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function create({ data, autoFormat = true }) {
  // create a new user
  const res = await commonService.create({
    Model: models.user,
    data,
    autoFormat,
  });
  return res;
}

async function encryptPassword(password) {
  const cipher = crypto.createCipher("aes-256-ctr", "d6F3Efeq");
  let crypted = cipher.update(password, "utf8", "hex");
  crypted += cipher.final("hex");
  return crypted;
}

async function decryptPassword(password) {
  const decipher = crypto.createDecipher("aes-256-ctr", "d6F3Efeq");
  let dec = decipher.update(password, "hex", "utf8");
  dec += decipher.final("utf8");
  return dec;
}

/**
 * Updating document
 * @property {object} Model Sequelize model object
 * @property {object} data - document which needs to be updated.
 * @property {document} id - document properties.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function updateExisting({ data, id, autoFormat = true }) {
  // updating the existing user
  const res = await commonService.updateExisting({
    Model: models.user,
    data,
    id,
    autoFormat,
  });
  return res;
}

/**
 * Checking if document exist with reference
 * @property {object} data - The user object .
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @property {object} models - sequelize Model object
 * @returns {boolean/document}
 */

async function checkDuplicate(data) {
  // Duplicate user if present.
  const res = await commonService.checkDuplicate({
    Model: models.user,
    data,
    autoFormat: true,
  });
  return res;
}

/**
 * delete row by id
 * @property {string} id - row id to be removed.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */

async function removeById({ id, autoFormat = true }) {
  // Remove user by its userId
  const res = await commonService.removeById({
    Model: models.user,
    id,
    autoFormat,
  });
  return res;
}

/**
 * Finding rows with id
 * @property {string} id - row id.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function findById({ id, autoFormat = true }) {
  // find user by its row id
  const res = await commonService.findById({
    Model: models.user,
    id,
    autoFormat,
  });
  return res;
}

export default {
  login,
  checkDuplicate,
  updateExisting,
  create,
  removeById,
  findById,
  encryptPassword,
  decryptPassword,
};
