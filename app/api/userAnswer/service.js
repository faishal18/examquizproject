import jwt from "jsonwebtoken";
import models from "../../database";
import util from "../../../utils/util";
import commonService from "../../helpers/services/commonService";
import messages from "../../../localization/en";

/**
 * Creating New Row
 * @property {object} data - New row properties.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function create(data) {
  const res = await commonService.create({
    Model: models.userAnswer,
    data,
    autoFormat: true,
  });
  return res;
}

/**
 * Updating the Row
 * @property {object} data - Row's properties.
 * @property {document} existingDoc - document which needs to be updated.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function updateExisting({ data, id, autoFormat = true }) {
  const existingDoc = await commonService.updateExisting({
    Model: models.userAnswer,
    data,
    id,
    autoFormat,
  });
  return existingDoc;
}

/**
 * Checking if document exist with reference
 * @property {object} data - The reference object .
 * @property {object}  - contains database models .
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {boolean/document}
 */

async function checkDuplicate(data, autoFormat = true) {
  const res = await commonService.checkDuplicate(data, models, autoFormat);
  return res;
}

async function removeById({ id, autoFormat = true }) {
  const res = await commonService.removeById({
    Model: models.userAnswer,
    id,
    autoFormat,
  });
  return res;
}

/**
 * Finding row with id
 * @property {string} id - document id.
 * @property {string} errKey - key for which error object will be generated.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function findById({ id, autoFormat = true }) {
  const res = await commonService.findById({
    Model: models.userAnswer,
    id,
    autoFormat,
  });
  return res;
}

/**
 * Finding rows with provided query params
 * @property {object} query - object containing params to prepare query.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {documents[]}
 */
async function find({ query, autoFormat = true }) {
  const res = await commonService.find({
    Model: models.userAnswer,
    query,
    autoFormat,
  });
  return res;
}

/**
 * Finding rows with provided query params
 * @property {object} id - contains id.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {documents[]}
 */
async function getQuestionsByTestId({ id, autoFormat = true }) {
  const filterCriteria = {
    where: {
      testId: id,
      is_active: true,
    },
    attributes: {
      exclude: [
        "rightChoice",
        "createdAt",
        "updatedAt",
        "testId",
        "is_active",
        "id",
      ],
    },
  };
  const res = await models.questions.findAll(filterCriteria);
  if (res !== null || res.length > 1) {
    // Returning formatted response if autoFormat true
    if (autoFormat) {
      return {
        // status: 200,
        data: res,
        message: messages.SUCCESSFULL,
      };
    }

    // Otherwise returned db object
    return res;
  }
  return res;
}

export default {
  checkDuplicate,
  updateExisting,
  create,
  removeById,
  findById,
  find,
  getQuestionsByTestId,
};
