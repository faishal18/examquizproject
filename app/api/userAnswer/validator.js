import Joi from "joi";
import constants from "../../helpers/constants";
import commonValidator from "../../helpers/validators/commonValidator";

export default {
  // GET /api/userAnswer/:id
  get: Joi.object({
    params: Joi.object({
      id: commonValidator.validMysqlId.required(),
    }),
  }),

  // GET /api/userAnswer
  list: Joi.object({
    query: Joi.object({
      sortBy: Joi.array().items(Joi.any().valid(constants.sortByKeys)),
      limit: Joi.number().integer(),
      skip: Joi.number().integer(),
      tags: Joi.array().items(
        Joi.any()
          .valid(constants.teamTags)
          .required(),
      ),
      approvalStatus: Joi.array().items(
        Joi.any()
          .valid(constants.approvalStatusTypes)
          .required(),
      ),
    }),
  }),

  // POST /api/userAnswer
  create: Joi.object({
    body: Joi.object({
      userId: commonValidator.validMysqlId.required(),
      questionId: commonValidator.validMysqlId.required(),
      choice: commonValidator.shortStr,
      isRight: Joi.number().integer(),
    }),
  }),

  // PUT /api/userAnswer/:id
  update: Joi.object({
    params: Joi.object({
      id: commonValidator.validMysqlId.required(),
    }),
    body: Joi.object({
      userId: commonValidator.validMysqlId.required(),
      questionId: commonValidator.validMysqlId.required(),
      choice: commonValidator.shortStr,
      isRight: Joi.number().integer(),
    }),
  }),

  // DELETE /api/userAnswer/:id
  remove: Joi.object({
    params: Joi.object({
      id: commonValidator.validMysqlId.required(),
    }),
  }),
};
