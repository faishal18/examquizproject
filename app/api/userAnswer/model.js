export default (sequelize, type) =>
  sequelize.define(
    "userAnswer",
    {
      id: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      questionId: type.INTEGER,
      userId: type.INTEGER,
      choice: type.STRING,
    },
    { freezeTableName: true },
  );
