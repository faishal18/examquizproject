import util from "../../../utils/util";
import validator from "./validator";
import service from "./service";

/**
 * Create new User
 * @property {string} body.userName - The name of team.
 * @property {string} body.emailId - The short name of team.
 * @property {string} body. - The display name of team.
 * @property {array} body.tags - Array of team tags.
 * @property {string} body.activeFeedSource - The current active feed source of team.
 * @property {string} body.reference - The reference object {feedSource: x, key:y} of team.
 * @property {string} body.frozen - true if data is finilized.
 * @property {string} body.approvalStatus - The approval status of team.
 * @property {string} body.avatar - The avatar of team.
 * @returns {Object}
 */

async function create(body) {
  // Validating body

  const validData = await validator.create.validate({ body });

  const validBody = validData.body;
  const promiseList = [];

  if (validBody.userName) {
    // Checking if document exist with same reference
    promiseList.push(service.checkDuplicate(validBody.userName));
  }

  // Waiting for promises to finish
  const promiseListResp = await Promise.all(promiseList);

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(promiseListResp);

  // Adding reference as references array
  // validBody.references = [validBody.reference];

  // Creating new team
  const newDoc = await service.create({ data: validBody });

  // Throwing error if promise response has any error object
  // util.FilterErrorAndThrow(newDoc);

  return newDoc;
}

/**
 * Get teams list.
 * @property {array} query.approvalStatus - Array of approval status.
 * @property {number} query.skip - Number of teams to be skipped.
 * @property {number} query.limit - Limit number of teams to be returned.
 * @property {array} query.tags - Array of team tags.
 * @property {array} query.sortBy - keys to use to record sorting.
 * @returns {Object}
 */
async function list(query) {
  // Validating query
  const validQuery = await validator.list.validate({ query });

  // Getting team list with filters
  const docs = await service.find({ query: validQuery.query });
  return docs;
}

async function remove(params) {
  // Validating param
  const validParam = await validator.remove.validate({ params });
  const { id } = validParam.params;
  // Updating status to deleted
  const deletedDoc = await service.removeById({ id });
  // const deletedDoc = "Hitting";

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(deletedDoc);

  return deletedDoc;
}

async function get(params) {
  const validParam = await validator.remove.validate({ params });
  const { id } = validParam.params;
  const foundDoc = await service.findById({ id });

  util.FilterErrorAndThrow(foundDoc);

  const finalDoc = await service.getQuestionsByTestId({ id });

  util.FilterErrorAndThrow(finalDoc);
  foundDoc.questons = finalDoc;
  return foundDoc;
}

/**
 * Update existing user
 * @property {string} params.id - Team Id.
 * @property {string} body.name - The name of team.
 * @property {string} body.shortName - The short name of team.
 * @property {string} body.displayName - The display name of team.
 * @property {array} body.tags - Array of team tags.
 * @property {string} body.activeFeedSource - The current active feed source of team.
 * @property {string} body.reference - The reference object {feedSource: x, key:y} of team.
 * @property {string} body.frozen - true if data is finilized.
 * @property {string} body.approvalStatus - The approval status of team.
 * @property {string} body.avatar - The avatar of team.
 * @returns {Object}
 */
async function update(params, body) {
  // Validating param
  const validParam = await validator.update.validate({ params });

  const { id } = validParam.params;

  // Getting team object to be updated
  const existingDoc = await service.findById({ id, autoFormat: false });

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(existingDoc);

  // Validating body
  const validData = await validator.update.validate({ body });

  const validBody = validData.body;
  const promiseList = [];

  // Waiting for promises to finish
  const promiseListResp = await Promise.all(promiseList);

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(promiseListResp);

  // Updating new data to document
  const savedDoc = await service.updateExisting({
    data: validBody,
    id,
  });

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(savedDoc);

  return savedDoc;
}

export default { create, remove, get, update, list };
