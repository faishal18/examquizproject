import Joi from "joi";
import { join } from "path";
import constants from "../../helpers/constants";
import commonValidator from "../../helpers/validators/commonValidator";

export default {
  // GET /api/user/:id
  get: Joi.object({
    params: Joi.object({
      id: commonValidator.validMysqlId,
    }),
  }),

  // GET /api/user
  list: Joi.object({
    query: Joi.object({
      sortBy: Joi.array().items(Joi.any().valid(constants.sortByKeys)),
      limit: Joi.number().integer(),
      skip: Joi.number().integer(),
      tags: Joi.array().items(
        Joi.any()
          .valid(constants.teamTags)
          .required(),
      ),
      approvalStatus: Joi.array().items(
        Joi.any()
          .valid(constants.approvalStatusTypes)
          .required(),
      ),
    }),
  }),

  // POST /api/user
  create: Joi.object({
    body: Joi.object({
      question: commonValidator.veryLongStr.required(),
      choices: Joi.object().required(),
      testId: Joi.number().integer(),
      rightChoice: commonValidator.shortStr.required(),
    }),
  }),

  // PUT /api/user/:id
  update: Joi.object({
    params: Joi.object({
      id: commonValidator.validMysqlId.required(),
    }),
    body: Joi.object({
      testName: commonValidator.normalStr.required(),
      testType: commonValidator.normalStr.required(),
    }),
  }),

  // DELETE /api/user/:id
  remove: Joi.object({
    params: Joi.object({
      id: commonValidator.validMysqlId.required(),
    }),
  }),
};
