export default (sequelize, type) => {
  const questions = sequelize.define(
    "questions",
    {
      id: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      question: type.STRING,
      is_active: type.INTEGER,
      choices: type.JSON,
      rightChoice: type.STRING,
      testId: type.INTEGER,
    },
    { freezeTableName: true },
  );
  questions.associate = models => {
    questions.belongsTo(models.test, { through: "testId" });
  };
  return questions;
};
