import express from "express";
import c from "../../../utils/controlHandler";
import controller from "./controller";

const router = express.Router();

router
  .route("/")
  // create new team (accessed at POST /api/test)
  .post(c(controller.create, ({ body }) => [body]))
  // list all test (accessed at GET /api/test)
  .get(c(controller.list, ({ query }) => [query]));

router
  .route("/:id([0-9]+)")
  // update team (accessed at PUT /api/test/:id)
  .put(c(controller.update, ({ params, body }) => [params, body]))
  // remove team (accessed at DELETE /api/test/:id)
  .delete(c(controller.remove, ({ params }) => [params]))
  // get team (accessed at GET /api/test/:id)
  .get(c(controller.get, ({ params }) => [params]));

export default router;
