import jwt from "jsonwebtoken";
import models from "../../database";
import util from "../../../utils/util";
import commonService from "../../helpers/services/commonService";
import messages from "../../../localization/en";

/**
 * Creating document
 * @property {object} data - document properties.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function create(data) {
  const res = await commonService.create({
    Model: models.questions,
    data,
    autoFormat: true,
  });
  return res;
}

/**
 * Updating document
 * @property {object} data - document properties.
 * @property {document} existingDoc - document which needs to be updated.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function updateExisting({ data, id, autoFormat = true }) {
  const res = await commonService.updateExisting({
    Model: models.questions,
    data,
    id,
    autoFormat,
  });
  return res;
}

async function checkDuplicate(questions) {
  const res = await commonService.checkDuplicate(questions, models);
  return res;
}

async function removeById({ id, autoFormat = true }) {
  const res = await commonService.removeById({
    Model: models.questions,
    id,
    autoFormat,
  });
  return res;
}

/**
 * Finding document with id
 * @property {string} id - document id.
 * @property {string} errKey - key for which error object will be generated.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function findById({ id, autoFormat = true }) {
  const res = await commonService.findById({
    Model: models.questions,
    id,
    autoFormat,
  });
  return res;
}

/**
 * Finding documents with provided query params
 * @property {object} query - object containing params to prepare query.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {documents[]}
 */
async function find({ query, autoFormat = true }) {
  const res = await commonService.find({
    Model: models.questions,
    query,
    autoFormat,
  });
  return res;
}

export default {
  checkDuplicate,
  updateExisting,
  create,
  removeById,
  findById,
  find,
};
