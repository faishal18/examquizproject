export default (sequelize, type) => {
  const test = sequelize.define("test", {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    testName: type.STRING,
    testType: type.STRING,
  });
  test.associate = models => {
    test.hasMany(models.questions);
  };
  return test;
};
