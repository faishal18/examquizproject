import util from "../../../utils/util";
import validator from "./validator";
import service from "./service";

/**
 * Create new test
 * @property {string} body.testName - The name of test.
 * @property {string} body.testType - The type of test.
 * @property {string} body.status - status false  if inactive.
 * @returns {Object}
 */

async function create(body) {
  // Validating body

  const validData = await validator.create.validate({ body });

  const validBody = validData.body;
  const promiseList = [];

  // Waiting for promises to finish
  const promiseListResp = await Promise.all(promiseList);

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(promiseListResp);

  // Creating new test
  const newDoc = await service.create({ data: validBody });

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(newDoc);

  return newDoc;
}

/**
 * Get teams list.
 * @property {number} query.skip - Number of teams to be skipped.
 * @property {number} query.limit - Limit number of teams to be returned.
 * @property {array} query.sortBy - keys to use to record sorting.
 * @returns {Object}
 */
async function list(query) {
  // Validating query
  const validQuery = await validator.list.validate({ query });

  // Getting test list with filters
  const docs = await service.find({ query: validQuery.query });
  return docs;
}

/**
 * Delete test.
 * @property {string} params.id - Team Id.
 * @returns {Object}
 */
async function remove(params) {
  // Validating param
  const validParam = await validator.remove.validate({ params });
  const { id } = validParam.params;
  // deleting test
  const deletedDoc = await service.removeById({ id });
  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(deletedDoc);

  return deletedDoc;
}

/**
 * Get Test
 * @property {string} params.id - test Id.
 * @returns {Object}
 */
async function get(params) {
  const validParam = await validator.remove.validate({ params });
  const { id } = validParam.params;
  const foundDoc = await service.findById({ id });

  util.FilterErrorAndThrow(foundDoc);

  const finalDoc = await service.getQuestionsByTestId({ id });

  util.FilterErrorAndThrow(finalDoc);
  foundDoc.questons = finalDoc;
  return foundDoc;
}

/**
 * Update existing test
 * @property {string} body.testName - The name of test.
 * @property {string} body.testType - The type of test.
 * @property {string} body.status - status false  if inactive.
 * @returns {Object}
 */
async function update(params, body) {
  // Validating param
  const validParam = await validator.update.validate({ params });

  const { id } = validParam.params;

  // Getting test object to be updated
  const existingDoc = await service.findById({ id, autoFormat: false });

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(existingDoc);

  // Validating body
  const validData = await validator.update.validate({ body });

  const validBody = validData.body;
  const promiseList = [];

  // Waiting for promises to finish
  const promiseListResp = await Promise.all(promiseList);

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(promiseListResp);

  // Updating new data to document
  const savedDoc = await service.updateExisting({
    data: validBody,
    id,
  });

  // Throwing error if promise response has any error object
  util.FilterErrorAndThrow(savedDoc);

  return savedDoc;
}

export default { create, remove, get, update, list };
