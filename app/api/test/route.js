import express from "express";
import c from "../../../utils/controlHandler";
import controller from "./controller";
import commonService from "../../helpers/services/commonService";

const router = express.Router();
// router.use(commonService.verifyToken, ({ headers }) => [headers]);
router
  .route("/")
  .post(commonService.verifyToken, c(controller.create, ({ body }) => [body]))
  // list all test (accessed at GET /api/test)
  .get(c(controller.list, ({ query }) => [query]));
// create new test (accessed at POST /api/test)
router
  .route("/:id([0-9]+)")
  // update test (accessed at PUT /api/test/:id)
  .put(c(controller.update, ({ params, body }) => [params, body]))
  // remove test (accessed at DELETE /api/test/:id)
  .delete(c(controller.remove, ({ params }) => [params]))
  // get test (accessed at GET /api/test/:id)
  .get(c(controller.get, ({ params }) => [params]));

export default router;
