import models from "../../database";
// import util from "../../../utils/util";
import commonService from "../../helpers/services/commonService";
import messages from "../../../localization/en";

/**
 * Creating document
 * @property {object} data - document properties.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function create({ data }) {
  const res = await commonService.create({
    Model: models.test,
    data,
    autoFormat: true,
  });
  return res;
}

/**
 * Updating document
 * @property {object} Model Sequelize model object
 * @property {object} data - document properties.
 * @property {document} existingDoc - document which needs to be updated.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function updateExisting({ data, id, autoFormat = true }) {
  const res = await commonService.updateExisting({
    Model: models.test,
    data,
    id,
    autoFormat,
  });
  return res;
}

/**
 * delete row by id
 * @property {string} id - row id to be removed.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */

async function removeById({ id, autoFormat = true }) {
  // Remove test by its userId
  const res = await commonService.removeById({
    Model: models.test,
    id,
    autoFormat,
  });
  return res;
}

/**
 * Finding rows with id
 * @property {string} id - row id.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {document}
 */
async function findById({ id, autoFormat = true }) {
  // find test by its row id
  const res = await commonService.findById({
    Model: models.test,
    id,
    autoFormat,
  });
  return res;
}

/**
 * Finding documents with provided query params
 * @property {object} query - object containing params to prepare query.
 * @property {boolean} autoFormat - false if formatted output not needed.
 * @returns {documents[]}
 */
async function find({ query, autoFormat = true }) {
  const res = await commonService.find({
    Model: models.test,
    query,
    autoFormat,
  });
  return res;
}
/**
 *
 * @property {object} id - testId
 * @property {boolean} autoformat - false if formatted output not needed.
 */

async function getQuestionsByTestId({ id, autoFormat = true }) {
  const filterCriteria = {
    where: {
      testId: id,
      is_active: true,
    },
    attributes: {
      exclude: ["rightChoice", "createdAt", "updatedAt", "testId", "is_active"],
    },
  };
  const res = await models.questions.findAll(filterCriteria);
  if (res !== null) {
    // Returning formatted response if autoFormat true
    if (autoFormat) {
      return {
        // status: 200,
        data: res,
        message: messages.SUCCESSFULL,
      };
    }

    // Otherwise returned db object
    return res;
  }
  return res;
}

export default {
  updateExisting,
  create,
  removeById,
  findById,
  find,
  getQuestionsByTestId,
};
