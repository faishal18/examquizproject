import Joi from "joi";
import { join } from "path";
import constants from "../../helpers/constants";
import commonValidator from "../../helpers/validators/commonValidator";

export default {
  // GET /api/test/:id
  get: Joi.object({
    params: Joi.object({
      id: commonValidator.validMysqlId,
    }),
  }),

  // GET /api/test
  list: Joi.object({
    query: Joi.object({
      sortBy: Joi.array().items(Joi.any().valid(constants.sortByKeys)),
      limit: Joi.number().integer(),
      skip: Joi.number().integer(),
      tags: Joi.array().items(
        Joi.any()
          .valid(constants.teamTags)
          .required(),
      ),
      approvalStatus: Joi.array().items(
        Joi.any()
          .valid(constants.approvalStatusTypes)
          .required(),
      ),
    }),
  }),

  // POST /api/test
  create: Joi.object({
    body: Joi.object({
      testName: commonValidator.normalStr.required(),
      testType: commonValidator.normalStr.required(),
    }),
  }),

  // PUT /api/test/:id
  update: Joi.object({
    params: Joi.object({
      id: commonValidator.validMysqlId.required(),
    }),
    body: Joi.object({
      testName: commonValidator.normalStr.required(),
      testType: commonValidator.normalStr.required(),
    }),
  }),

  // DELETE /api/test/:id
  remove: Joi.object({
    params: Joi.object({
      id: commonValidator.validMysqlId.required(),
    }),
  }),
};
