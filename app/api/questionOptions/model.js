export default (sequelize, type) => {
  const questionOptions = sequelize.define(
    "questionOptions",
    {
      id: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      choices: type.JSON,
      rightChoice: type.INTEGER,
      questionId: type.INTEGER,
    },
    { freezeTableName: true },
  );
  questionOptions.associate = models => {
    questionOptions.belongsTo(models.questions);
  };
  return questionOptions;
};
