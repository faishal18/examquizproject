import express from "express";

import teamRoutes from "./app/api/team/route";
import userRoutes from "./app/api/user/route";
import testRoutes from "./app/api/test/route";
import questionsRoutes from "./app/api/questions/route";
import userAnswersRoutes from "./app/api/userAnswer/route";
const router = express.Router();

/** GET /health-check - Check service health */
router.get("/health-check", (req, res) => res.send("OK"));

// mount sample routes at /sample
router.use("/teams", teamRoutes);

router.use("/user", userRoutes);

router.use("/test", testRoutes);

router.use("/questions", questionsRoutes);

router.use("/userAnswer", userAnswersRoutes);

export default router;
